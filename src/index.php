<?php

require_once __DIR__ . '/../vendor/autoload.php';


$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/content',
));
$app->register(new Silex\Provider\RoutingServiceProvider());


$app['debug'] = false;
$app['asset_path'] = $_ENV["SERVER_NAME"];

$app['vanilla_forum_url'] = $_ENV["VANILLA_FORUM_URL"];
$app['vanilla_category_id'] = $_ENV["VANILLA_CATEGORY_ID"];


/**
  * Domovská stránka
  */
$app->get('/', function () use ($app) {
    return $app['twig']->render('homepage.html');
})->bind('homepage');


/**
  * Seznam aktuálních povídek
  */
$app->get('/povidky/soutezni/', function () use ($app) {
    return $app['twig']->render('povidky/aktualni.html');
})->bind('povidky-soutezni');


/**
  * Seznam předchozích povídek
  */
$app->get('/povidky/predchozi/', function () use ($app) {
    return $app['twig']->render('povidky/predchozi.html');
})->bind('povidky-predchozi');


/**
  * Seznam nesoutěžních povídek
  */
$app->get('/povidky/nesoutezni/', function () use ($app) {
    return $app['twig']->render('povidky/nesoutezni.html');
})->bind('povidky-nesoutezni');


/**
  * Seznam historických povídek (z IF)
  */
$app->get('/povidky/historicke/', function () use ($app) {
    return $app['twig']->render('povidky/historicke.html');
})->bind('povidky-historicke');


/**
  * Stránka s historickou povídkou
  */
$app->get('/povidky/historicke/{kolo}/{nazev}', function ($kolo, $nazev) use ($app) {
    $template = 'povidky/historicke/' . $kolo . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('povidky-historicke-kus');


/**
  * Stránka s nesoutěžní povídkou
  */
$app->get('/povidky/nesoutezni/{svet}/{nazev}', function ($svet, $nazev) use ($app) {
    $template = 'povidky/nesoutezni/' . $svet . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('povidky-nesoutezni-kus');


/**
  * Stránka se soutěžní povídkou
  */
$app->get('/povidky/soutezni/{kolo}/{nazev}', function ($kolo, $nazev) use ($app) {
    $template = 'povidky/soutezni/' . $kolo . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('povidky-soutezni-kus');


/**
  * Seznam aktuálních drabblat
  */
$app->get('/drabble/', function () use ($app) {
    return $app['twig']->render('drabble/aktualni.html');
})->bind('drabble-soutezni');


/**
  * Seznam předchozích drabblat
  */
$app->get('/drabble/predchozi/', function () use ($app) {
    return $app['twig']->render('drabble/predchozi.html');
})->bind('drabble-predchozi');


/**
  * Stránka s adventními kalendáři
  */
$app->get('/drabble/adventni/', function () use ($app) {
    // $drabbles2015 = array();
    // $home = '/home/apps/app_00217/app/';
    // $public = $home . 'content/drabble/adventni/2015/';
    // $private = $home . 'content/neverejne/ldoeguhtoewpdoiv234-drabble2015/';

    // foreach (scandir($public) as $drabble) {
    //     $nazev = explode('.', $drabble)[0];
    //     array_push($drabbles2015, $nazev);
    // }

    // $day = date('j');
    // if (!in_array($day, $drabbles2015)) {
    //     rename($private . $day . '.html', $public . $day . '.html');
    //     array_push($drabbles2015, $day);
    // }

    // $app['twig']->addGlobal('published', $drabbles2015);
    return $app['twig']->render('drabble/adventni.html');
})->bind('drabble-adventni');


/**
  * Stránka se soutěžním drabble
  */
$app->get('/drabble/soutezni/{kolo}/{nazev}', function ($kolo, $nazev) use ($app) {
    $template = 'drabble/soutezni/' . $kolo . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('drabble-soutezni-kus');


/**
  * Stránka s adventním drabble
  */
$app->get('/drabble/adventni/{rok}/{nazev}', function ($rok, $nazev) use ($app) {
    $template = 'drabble/adventni/' . $rok . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('drabble-adventni-kus');


/**
  * Nezveřejněný obsah
  */
$app->get('/neverejne/{slozka}/{nazev}', function ($slozka, $nazev) use ($app) {
    $template = 'neverejne/' . $slozka . '/' . $nazev . '.html';
    return $app['twig']->render($template);
})->bind('neverejny-obsah');


/**
  * Fórum Skokanůotyči
  */
$app->get('/forum/', function () use ($app) {
    return $app->redirect($app['asset_path'] . 'forum');
})->bind('forum');


/**
  * Mapy
  */
$app->get('/mapy/', function () use ($app) {
    return $app['twig']->render('mapy.html');
})->bind('mapy');


$app->run();
