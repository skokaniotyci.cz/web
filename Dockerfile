FROM php:7.3-apache

EXPOSE 8000

WORKDIR /tmp/build-deps

COPY scripts/composer-install.sh composer.lock composer.json /tmp/build-deps/

RUN useradd app \
    && usermod -d /var/www/html app \
    && apt-get update && apt-get install -y git unzip wget \
    && ./composer-install.sh \
    && php composer.phar install \
    && mv vendor /var/www/ \
    && a2enmod rewrite \
    && apt-get purge -y git unzip wget \
    && rm -rf /tmp/build-deps

USER app

WORKDIR /var/www/html

COPY src/ /var/www/html
COPY apache2/sites-available/000-default.conf /etc/apache2/sites-available/
COPY apache2/ports.conf /etc/apache2/

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
